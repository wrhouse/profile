"""empty message

Revision ID: ccb2456c75b5
Revises: 6055826f6d30
Create Date: 2019-07-26 21:31:45.225484

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = 'ccb2456c75b5'
down_revision = '6055826f6d30'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('email_verified', sa.Boolean(), nullable=True))
    op.add_column('users', sa.Column('phone_verified', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'phone_verified')
    op.drop_column('users', 'email_verified')
    # ### end Alembic commands ###
