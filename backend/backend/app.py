import base64
from pathlib import Path
from typing import Optional, List

import aiohttp_jinja2
import aiopg.sa
import asyncio_redis
import fernet
import jinja2
from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp_swagger import *
from sqlalchemy_utils import force_auto_coercion

from backend import constants
from backend.routes import init_routes
from backend.utils.common import init_config

path = Path(__file__).parent


def init_jinja2(app: web.Application) -> None:
    """
    Initialize jinja2 template for application.
    """
    aiohttp_jinja2.setup(
        app, loader=jinja2.FileSystemLoader(str(path / "templates"))
    )


async def database(app: web.Application) -> None:
    """
    A function that, when the server is started, connects to postgresql,
    and after stopping it breaks the connection (after yield)
    """
    config = app["config"]["postgres"]
    force_auto_coercion()
    engine = await aiopg.sa.create_engine(**config)
    app["db"] = engine
    yield

    app["db"].close()
    await app["db"].wait_closed()


async def redis_con(app: web.Application):
    """
    Connects to redis on startup.
    Breaks connection on cleanup.
    """
    config = app["config"]["redis"]
    connection = await asyncio_redis.Connection.create(**config)
    app["redis"] = connection

    yield

    app["redis"].close()


def init_app(config: Optional[List[str]] = None) -> web.Application:
    app = web.Application()
    # Setting up aiohttp_sessions.
    fernet_key = fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(fernet_key)
    setup(app, EncryptedCookieStorage(secret_key))
    init_jinja2(app)
    init_config(app, config=config)
    init_routes(app)
    if constants.DEBUG_MODE:
        setup_swagger(app, swagger_url="/api/v1/doc")
    app.cleanup_ctx.extend([database, redis_con])

    return app
