import argparse
import os
import pathlib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Any, Optional, List

import aiosmtplib
import trafaret
from aiohttp import web
from trafaret_config import commandline

PATH = pathlib.Path(__file__).parent.parent.parent
settings_file = os.environ.get("SETTINGS_FILE", "api.dev.yml")
DEFAULT_CONFIG_PATH = PATH / "config" / settings_file

CONFIG_TRAFARET = trafaret.Dict(
    {
        trafaret.Key("app"): trafaret.Dict(
            {"host": trafaret.String(), "port": trafaret.Int()}
        ),
        trafaret.Key("postgres"): trafaret.Dict(
            {
                "host": trafaret.String(),
                "port": trafaret.Int(),
                "user": trafaret.String(),
                "password": trafaret.String(),
                "database": trafaret.String(),
            }
        ),
        trafaret.Key("redis"): trafaret.Dict(
            {"host": trafaret.String(), "port": trafaret.Int()}
        ),
        trafaret.Key("mail"): trafaret.Dict(
            {
                trafaret.Key("connection"): trafaret.Dict({
                    "hostname": trafaret.String(),
                    "port": trafaret.Int(),
                    "use_tls": trafaret.Bool()
                }
                ),
                trafaret.Key("credentials"): trafaret.Dict({
                    "username": trafaret.String(),
                    "password": trafaret.String()
                }
                )
            }
        ),
    }
)


def get_config(argv: Any = None) -> Any:
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(
        ap, default_config=DEFAULT_CONFIG_PATH
    )
    options = ap.parse_args(argv)

    return commandline.config_from_options(options, CONFIG_TRAFARET)


def init_config(
        app: web.Application, *, config: Optional[List[str]] = None
) -> None:
    app["config"] = get_config(
        config or ["-c", DEFAULT_CONFIG_PATH.as_posix()]
    )


async def send_email(app: web.Application, to_list: List[str], message_text: str, subject: str = None):
    mail_settings = app['config']['mail']
    message = MIMEMultipart('alternative')
    sender = f"Gaming Warehouse Profile<{app['config']['mail']['credentials']['username']}>"
    message['From'] = sender
    message['To'] = ", ".join(to_list)
    message['Subject'] = subject if subject is not None else "Gaming Warehouse notification"
    message.attach(MIMEText(message_text, 'html'))
    async with aiosmtplib.SMTP(**mail_settings['connection']) as smtp:
        await smtp.login(**mail_settings['credentials'])
        await smtp.sendmail(sender, to_list, message.as_string())
