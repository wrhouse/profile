from aiohttp import web


def success_response(data):
    response = {
        "success": True,
        "data": data
    }
    return web.json_response(response)


def error_response(message, code=400, error_code=None):
    if error_code is None:
        error_code = code
    response = {
        "success": False,
        "error": {
            "code": error_code,
            "message": message
        }
    }
    return web.json_response(response, status=code)
