from functools import wraps

import jwt
from aiohttp import web

from backend import constants


def protected_cookie(f):
    @wraps(f)
    async def protected_func(request: web.Request, *args, **kwargs):
        token = request.cookies.get("access_token")
        if token is None:
            return web.Response(text="No token was provided.", status=401)
        return await f(request, *args, **kwargs)

    return protected_func


def protected_query(f):
    @wraps(f)
    async def protected_func(request: web.Request, *args, **kwargs):
        token = request.rel_url.query.get("access_token")
        if token is None:
            return web.Response(text="No token was provided.", status=401)
        session_info = jwt.decode(token, constants.PROFILE_SECRET)
        return await f(request, session_info=session_info, *args, **kwargs)

    return protected_func
