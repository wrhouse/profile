import asyncio
import random
import socket
import time

import asyncio_redis

from backend.utils.common import get_config, DEFAULT_CONFIG_PATH

if __name__ == "__main__":
    config = get_config(["-c", DEFAULT_CONFIG_PATH.as_posix()])
    postgres = config["postgres"]
    redis = config['redis']
    postgres_connected = "FAILED"
    redis_connected = "FAILED"
    while True:
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                asyncio.get_event_loop().run_until_complete(
                    asyncio_redis.Connection.create(host=redis['host'], port=redis['port']))
                redis_connected = "  OK  "
                sock.connect((postgres["host"], postgres["port"]))
                postgres_connected = "  OK  "
                print("[  OK  ] Postgresql connected")
                print("[  OK  ] Redis connected")
                break
        except socket.error:
            print(f"[{postgres_connected}] Postgresql connected")
            print(f"[{redis_connected}] Redis connected")
            time.sleep(0.5 + (random.randint(0, 100) / 1000))
