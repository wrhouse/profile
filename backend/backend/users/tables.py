import sqlalchemy as sa
import sqlalchemy_utils

from backend.migrations import Base
from backend.users.enums import (
    UserTypes,
    GenderTypes
)

sqlalchemy_utils.force_auto_coercion()


# __all__ = ["users"]
class users(Base):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    nickname = sa.Column(sa.String(), unique=True)
    forumId = sa.Column(sa.Integer())
    shopId = sa.Column(sa.Integer)
    vkId = sa.Column(sa.String())
    googleId = sa.Column(sa.String())
    sessionId = sa.Column(sa.String())
    twitterId = sa.Column(sa.String())
    facebookId = sa.Column(sa.String())
    name = sa.Column(sa.String())
    surname = sa.Column(sa.String())
    email = sa.Column(sa.String(), unique=True)
    email_verified = sa.Column(sa.Boolean())
    password = sa.Column(sa.String())
    birth = sa.Column(sa.Date())
    social = sa.Column(sa.JSON())
    phone = sa.Column(sa.String())
    phone_verified = sa.Column(sa.Boolean())
    tfa = sa.Column(sa.Boolean())
    type = sa.Column(sa.Enum(UserTypes), server_default=UserTypes.user.value)
    gender = sa.Column(sa.Enum(GenderTypes), server_default=GenderTypes.undefined.value)
