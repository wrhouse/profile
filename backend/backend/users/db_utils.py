from typing import Union

import sqlalchemy as sa
from aiopg.sa import SAConnection, Engine
from aiopg.sa.result import RowProxy
from passlib.context import CryptContext
from sqlalchemy import select
from sqlalchemy.orm.attributes import InstrumentedAttribute

from backend.users.tables import users

pwd_context = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=30000
)


async def select_user_by_id(conn: SAConnection, key: int) -> RowProxy:
    users_table = users.__table__
    query = users_table.select().where(users.id == key).order_by(users.id)
    cursor = await conn.execute(query)
    return await cursor.fetchone()


async def find_user(engine: Engine, email: str, password: str) -> Union[users, None]:
    async with engine.acquire() as conn:
        query = sa.select([users, users]).where(users.email == email)
        res = await conn.execute(query)
        users_res = await res.fetchone()
        if pwd_context.verify(password, users_res['password']):
            return users(**users_res)
        return None


async def add_user(engine: Engine, user: dict):
    async with engine.acquire() as conn:
        user_table = users.__table__
        user['password'] = pwd_context.encrypt(user['password'])
        await conn.execute(user_table.insert().values(**user))


async def get_user_by_social(engine: Engine, social: InstrumentedAttribute, social_id: str):
    async with engine.acquire() as conn:
        query = select([users]).where(social == str(social_id))
        res = await conn.execute(query)
        user = await res.fetchone()
        return user
