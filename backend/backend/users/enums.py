from enum import Enum


class UserTypes(Enum):
    user = "user"
    admin = "admin"
    seller = "seller"


class GenderTypes(Enum):
    male = "male"
    female = "female"
    undefined = "undefined"
