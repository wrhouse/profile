import pathlib

from aiohttp import web

from backend.main.api import routes as api_urls
from backend.main.views import index
from backend.utils.urls import include_urls

PROJECT_PATH = pathlib.Path(__file__).parent


def init_routes(app: web.Application) -> None:
    app.router.add_route("*", "/description", index, name="index")
    include_urls(app, "/api", api_urls)
