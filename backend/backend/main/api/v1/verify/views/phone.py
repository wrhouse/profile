from aiohttp import web

from backend.utils.protection import protected_cookie


@protected_cookie
async def start_verification(request: web.Request):
    pass


@protected_cookie
async def end_verification(request: web.Request):
    pass
