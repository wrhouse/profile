from aiohttp import web

from backend.main.api.v1.verify.views.email import (
    start_verification,
    end_verification
)


def init_routes(app: web.Application, prefix: str):
    app.router.add_get(prefix + "/email", start_verification)
    app.router.add_post(prefix + "/email", end_verification)
    pass
