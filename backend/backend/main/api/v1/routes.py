from aiohttp import web

from backend.main.api.v1.account import routes as account_routes
from backend.main.api.v1.auth import routes as auth_routes
from backend.main.api.v1.session import routes as session_routes
from backend.main.api.v1.verify import routes as verify_routes
from backend.utils.urls import include_urls


def init_routes(app: web.Application, prefix: str):
    include_urls(app, prefix + "/auth", auth_routes)
    include_urls(app, prefix + "/session", session_routes)
    include_urls(app, prefix + "/account", account_routes)
    include_urls(app, prefix + "/verify", verify_routes)
