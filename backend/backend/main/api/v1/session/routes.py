from aiohttp import web

from backend.main.api.v1.session.views.logout import logging_out, logout_redirector
from backend.main.api.v1.session.views.starting import (
    session_create,
    session_redirects
)


def init_routes(app: web.Application, prefix: str):
    app.router.add_route('GET', prefix + '/logout', logging_out)
    app.router.add_route('GET', prefix + '/redirects/login', session_redirects)
    app.router.add_route('GET', prefix + '/create', session_create)
    app.router.add_route('GET', prefix + '/redirects/logout', logout_redirector)
