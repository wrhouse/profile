from urllib.parse import urlencode

import jwt
from aiohttp import web
from jsonschema import validate, ValidationError

from backend import constants
from backend.utils.responses import error_response


async def session_redirects(request: web.Request):
    """
    ---
    description: This end-point helps to continue redirect chain.
    tags:
    - Session
    parameters:
    -   in: query
        name: tokens
        description: Services tokens.
        schema:
            type: string
        required: true
    -   in: query
        name: service_id
        description: Service where cookies was set.
        schema:
            type: string
        required: true
    """
    schema = {
        "type": "object",
        "properties": {
            "tokens": {
                "type": "string"
            },
            "service_id": {
                "type": "string"
            }
        },
        "required": ["tokens"]
    }
    query = dict(request.query)
    try:
        validate(instance=query, schema=schema)
    except ValidationError as err:
        return error_response({
            "errors": err.message
        })
    tokens_data = jwt.decode(query['tokens'], constants.PROFILE_SECRET)
    tokens_array = tokens_data['tokens']
    for i, el in enumerate(tokens_array):
        if el['service_id'] == query['service_id']:
            print(f"removing {el['service_id']}")
            tokens_array.pop(i)
            break
    if len(tokens_array) == 0:
        return web.HTTPFound(f"https://{constants.HOST}")
    next_token = tokens_array[0]
    refresh = next_token['refresh_token']
    access = next_token['access_token']
    next_service = next_token['service_id']
    tokens_array = jwt.encode({"tokens": tokens_array}, constants.PROFILE_SECRET).decode("utf-8")
    params = {
        "tokens": tokens_array,
        "refresh_token": refresh,
        "access_token": access
    }
    return web.HTTPFound(
        f"{constants.SERVICE_URLS[next_service]}/cookies?{urlencode(params)}"
    )


async def session_create(request: web.Request):
    """
    ---
    description: This method creates tokens and start redirect chain.
    tags:
    - Session
    parameters:
    -   in: query
        name: tokens
        description: Services tokens.
        schema:
            type: string
        required: true
    """
    schema = {
        "type": "object",
        "properties": {
            "tokens": {
                "type": "string"
            }
        },
        "required": ["tokens"]
    }
    query = dict(request.query)
    try:
        validate(instance=query, schema=schema)
    except ValidationError as err:
        return error_response({
            "errors": err.message
        })
    tokens_data = jwt.decode(query['tokens'], constants.PROFILE_SECRET)
    from pprint import pprint
    pprint(tokens_data)
    for el in tokens_data['tokens']:
        if el['service_id'] == "profile":
            params = {
                "tokens": query['tokens'],
                "access_token": el['access_token'],
                "refresh_token": el["refresh_token"]
            }
            return web.HTTPFound(f"/cookies?{urlencode(params)}")
