import jwt
from aiohttp import web, ClientSession
from jsonschema import validate, ValidationError

from backend import constants
from backend.utils.protection import protected_query
from backend.utils.responses import error_response


@protected_query
async def logging_out(request: web.Request, session_info):
    async with ClientSession() as session:
        async with session.get(f"{constants.TOKEN_SERVICE}/api/session/{session_info['session_id']}") as resp:
            resp_json = await resp.json()
            user_id = resp_json.get("data", {}).get("user_id")

        payload = {
            "user_id": user_id,
            "whitelist": []
        }

        async with session.delete(
                f'{constants.TOKEN_SERVICE}/api/session?user_id={user_id}',
                json=payload
        ) as response:
            if response.status != 200:
                return error_response("Can't perform logout")
    services = jwt.encode({"services": list(constants.SERVICE_URLS.keys())}, constants.PROFILE_SECRET).decode("utf-8")
    return web.HTTPFound(f"{constants.APP_HOST}/cookiesRemove?services={services}")


async def logout_redirector(request: web.Request):
    schema = {
        "type": "object",
        "properties": {
            "services": {
                "type": "string"
            },
            "service_id": {
                "type": "string"
            }
        },
        "required": ["services", "service_id"]
    }
    query = dict(request.query)
    try:
        validate(instance=query, schema=schema)
    except ValidationError as err:
        return error_response({
            "errors": err.message
        })
    services = jwt.decode(request.query.get("services"), constants.PROFILE_SECRET)
    current = request.query.get('service_id')
    services.get('services').remove(current)
    if len(services.get('services')) == 0:
        return web.HTTPFound(f"https://{constants.HOST}")
    next = services.get('services')[0]
    updated_services = jwt.encode(services, constants.PROFILE_SECRET).decode("utf-8")
    return web.HTTPFound(f"{constants.SERVICE_URLS[next]}/cookiesRemove?services={updated_services}")
