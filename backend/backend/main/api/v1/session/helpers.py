# {
#   "user_id": 0,
#   "shop_user_id": 0,
#   "forum_user_id": 0,
#   "name": "string",
#   "surname": "string",
#   "nickname": "string",
#   "email": "user@example.com",
#   "email_is_verified": true,
#   "phone": "string",
#   "phone_is_verified": true,
#   "password": "string"
# }
import jwt
from aiohttp import ClientSession
from aiopg.sa import Engine

from backend import constants
from backend.users.tables import users


async def get_services_tokens(engine: Engine, user: users):
    formatted_user = format_user(user)
    async with ClientSession() as session:
        async with session.post(constants.TOKEN_SERVICE + "/api/session", json=formatted_user) as resp:
            if resp.status != 200:
                raise Exception("Bad session.")
            response = await resp.json()
            session_id = response.get("data", {}).get("session_id")
            if session_id is None:
                raise Exception("No session_id provided.")
            async with engine.acquire() as conn:
                user_table = users.__table__
                query = user_table.update().where(users.id == user.id).values(sessionId=session_id)
                await conn.execute(query)

        token_payload = {
            "session_id": session_id,
            "service_ids": list(constants.SERVICE_URLS.keys())
        }
        async with session.post(constants.TOKEN_SERVICE + "/api/token", json=token_payload) as resp:
            tokens = await resp.json()
            return jwt.encode(tokens['data'], constants.PROFILE_SECRET).decode("utf-8")


def format_user(user: users):
    return {
        "user_id": user.id,
        # "shop_user_id": user.shopId,
        # "forum_user_id": user.forumId,
        "name": user.name,
        "surname": user.surname,
        "nickname": user.nickname or f"user_{user.id}",
        "email": user.email,
        "email_is_verified": user.email_verified or False,
        "phone": user.phone or "",
        "phone_is_verified": user.phone_verified or False,
        "password": user.password
    }
