from aiohttp import web

from backend.main.api.v1.account.views.manage import create


def init_routes(app: web.Application, prefix: str):
    app.router.add_post(prefix + "/create", create)
