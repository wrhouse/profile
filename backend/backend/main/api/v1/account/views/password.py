from aiohttp import web

from backend.utils.protection import protected_cookie


@protected_cookie
def change(request: web.Request):
    pass


def recover(request: web.Request):
    pass


def recover_confirm(request: web.Request):
    pass
