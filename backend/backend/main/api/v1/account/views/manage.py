from aiohttp import web
from jsonschema import (
    validate,
    FormatChecker,
    ValidationError
)

from backend.users.db_utils import add_user
from backend.utils.responses import error_response, success_response

user_schema = {
    "type": "object",
    "properties": {
        "email": {"type": "string", "format": "email"},
        "password": {"type": "string"},
        "name": {"type": "string"},
        "surname": {"type": "string"},
        "vkId": {"type": "string"},
        "facebookId": {"type": "string"},
        "googleId": {"type": "string"},
        "twitterId": {"type": "string"}
    },
    "required": [
        "email",
        "password",
        "name",
        "surname"
    ]
}


async def create(request: web.Request):
    """
    ---
    description: Create new user with provided info.
    tags:
    - Account
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
      - in: body
        name: user_data
        description: user object to create
        required: true
        schema:
            type: object
            properties:
                email:
                    type: string
                    format: email
                    example: win10@list.ru
                password:
                    type: string
                    example: password
                name:
                    type: string
                    example: John
                surname:
                    type: string
                    example: Doe
                vkId:
                    type: string
                googleId:
                    type: string
                twitterId:
                    type: string
                facebookId:
                    type: string
    responses:
        "200":
            description: successful operation.
        "400":
            description: wrong body schema
    """
    user_data = await request.json()
    try:
        validate(instance=user_data,
                 schema=user_schema,
                 format_checker=FormatChecker())
    except ValidationError as error:
        return error_response({
            "errors": error.message
        })
    db = request.app['db']
    try:
        await add_user(db, user_data)
    except Exception as e:
        return error_response(str(e))
    return success_response("User created")
