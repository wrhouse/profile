from aiohttp import web

import backend.main.api.v1.auth.url.routes as url_routes
from backend.main.api.v1.auth.views import (
    vk_callback,
    google_callback,
    twitter_callback,
    fb_callback,
    plain
)
from backend.utils.urls import include_urls


def init_routes(app: web.Application, prefix: str):
    include_urls(app, prefix + "/url", url_routes)
    app.router.add_get(prefix + '/vk', vk_callback)
    app.router.add_get(prefix + '/google', google_callback)
    app.router.add_get(prefix + '/twitter', twitter_callback)
    app.router.add_get(prefix + '/fb', fb_callback)
    app.router.add_get(prefix + '/plain', plain)
