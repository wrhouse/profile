import urllib.parse
import urllib.parse
from urllib.parse import urlencode

import oauth2 as oauth
from aiohttp import web

from backend import constants


async def fb_init(request: web.Request):
    """
    ---
    description: Start authentication with Facebook.
    tags:
    - Social_init
    responses:
        "302":
            description: start redirection to Facebook oauth.
    """
    params = {
        "client_id": constants.FB_APP_ID,
        "redirect_uri": f"{constants.APP_HOST}/api/v1/auth/fb",
        "state": "email"
    }
    return web.HTTPFound(f"https://www.facebook.com/v3.3/dialog/oauth?{urlencode(params)}")


async def google_init(request: web.Request):
    """
    ---
    description: Start authentication with google.
    tags:
    - Social_init
    responses:
        "302":
            description: start redirection to google oauth.
    """
    params = {
        "client_id": constants.GOOGLE_CLIENT_ID,
        "response_type": "code",
        "scope": "openid%20email",
        "redirect_uri": f"{constants.APP_HOST}/api/v1/auth/google",
        "nonce": "0394852-3190485-2490358",
        # "hd": "localhost:8080"
        "hd": "gaming-warehousedev.com"
    }
    return web.HTTPFound(f"https://accounts.google.com/o/oauth2/v2/auth?{urlencode(params)}")


async def vk_init(request: web.Request):
    """
    ---
    description: Start authentication with VK.
    tags:
    - Social_init
    responses:
        "302":
            description: start redirection to vk oauth.
    """
    params = {
        "client_id": constants.VK_APP_ID,
        "display": "popup",
        "redirect_uri": f"{constants.APP_HOST}/api/v1/auth/vk",
        "scope": "email",
        "response_type": "code",
        "v": "5.95"
    }
    return web.HTTPFound(
        f"https://oauth.vk.com/authorize?{urlencode(params)}"
    )


async def twitter_init(request: web.Request):
    """
    ---
    description: Start authentication with VK.
    tags:
    - Social_init
    responses:
        "302":
            description: start redirection to twitter oauth.
    """
    request_token_url = f'https://api.twitter.com/oauth/request_token'
    authenticate_url = 'https://api.twitter.com/oauth/authenticate'
    consumer = oauth.Consumer(constants.TWITTER_API_KEY, constants.TWITTER_API_SECRET)
    client = oauth.Client(consumer)
    resp, content = client.request(request_token_url, "POST")
    if resp['status'] != '200':
        return web.Response(text="Invalid response from Twitter.")
    request_token = dict(urllib.parse.parse_qsl(content.decode('utf-8')))
    # request.session['request_token'] = request_token
    return web.HTTPFound(f"{authenticate_url}?oauth_token={request_token['oauth_token']}")
