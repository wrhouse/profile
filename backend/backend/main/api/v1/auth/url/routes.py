from aiohttp import web

from backend.main.api.v1.auth.url.views import (
    vk_init,
    fb_init,
    google_init,
    twitter_init
)


def init_routes(app: web.Application, prefix: str):
    app.router.add_get(prefix + '/vk', vk_init)
    app.router.add_get(prefix + '/fb', fb_init)
    app.router.add_get(prefix + '/google', google_init)
    app.router.add_get(prefix + '/twitter', twitter_init)
