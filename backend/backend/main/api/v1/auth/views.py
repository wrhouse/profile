import aiohttp
import jwt
from aiohttp import web
from jsonschema import (
    validate,
    FormatChecker,
    ValidationError

)

from backend import constants
from backend.main.api.v1.session.helpers import get_services_tokens
from backend.users.db_utils import find_user, get_user_by_social
from backend.users.tables import users
from backend.utils.responses import error_response


async def vk_callback(request: web.Request):
    """
    ---
    description: VK authentication callback.
    tags:
    - Auth
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
      - in: query
        name: code
    responses:
        "302":
            description: start redirection to vk oauth.
        "404":
            description: user was not found.
    """
    code = request.rel_url.query.get('code')
    if code is None:
        return error_response("Missing required query parameter", 422)
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(
                    f'https://oauth.vk.com/access_token',
                    params={
                        'client_id': constants.VK_APP_ID,
                        'client_secret': constants.VK_SECRET,
                        'redirect_uri': f'{constants.APP_HOST}/api/v1/auth/vk',
                        'code': code}) as resp:
                user_json = await resp.json()
                if user_json.get('error') is not None:
                    return error_response(user_json['error_description'])
                user_id = user_json['user_id']
                email = user_json['email']
                user = await get_user_by_social(request.app['db'], users.vkId, user_id)
                if user is None:
                    return error_response("User was not found")
                jwt_encoded_tokens = await get_services_tokens(request.app['db'], user)
                return web.HTTPFound(f"/api/v1/session/create?tokens={jwt_encoded_tokens}")
        except Exception as e:
            return error_response(str(e))


async def twitter_callback(request: web.Request):
    return web.Response(text=str(request.rel_url.query))


async def google_callback(request: web.Request):
    """
    ---
    description: Google authentication callback.
    tags:
    - Auth
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
      - in: query
        name: code
    responses:
        "302":
            description: start session.
        "404":
            description: user was not found.
    """
    try:
        code = request.rel_url.query.get('code')
        if code is None:
            return error_response("Missing required query parameter.", 422)
        async with aiohttp.ClientSession() as session:
            async with session.post("https://www.googleapis.com/oauth2/v4/token",
                                    data={'code': code,
                                          'client_id': constants.GOOGLE_CLIENT_ID,
                                          'client_secret': constants.GOOGLE_SECRET,
                                          'redirect_uri': f"{constants.APP_HOST}/api/v1/auth/google",
                                          'grant_type': 'authorization_code'}) as google_response:
                user_json = await google_response.json()
                if user_json.get("error") is not None:
                    return error_response(user_json.get('error_description'))
                user = jwt.decode(user_json.get('id_token'), verify=False)
                email = user.get('email')
                sub = user.get('sub')
                verified = user.get('email_verified', False)
                user = await get_user_by_social(request.app['db'], users.googleId, sub)
                if user is None:
                    return error_response("User was not found")
                jwt_encoded_tokens = await get_services_tokens(request.app['db'], user)
                return web.HTTPFound(f"/api/v1/session/create?tokens={jwt_encoded_tokens}")
    except Exception as e:
        return error_response(str(e))


async def fb_callback(request: web.Request):
    """
    ---
    description: Facebook authentication callback.
    tags:
    - Auth
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
      - in: query
        name: code
    responses:
        "302":
            description: start session.
        "404":
            description: user was not found.
    """
    code = request.rel_url.query.get('code')
    if code is None:
        return error_response("Missing required query parameter", 422)
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get("https://graph.facebook.com/v3.3/oauth/access_token",
                                   params={"client_id": constants.FB_APP_ID,
                                           "redirect_uri": f"{constants.APP_HOST}/api/v1/auth/fb",
                                           "client_secret": constants.FB_SECRET,
                                           "code": code}) as resp:
                token_json = await resp.json()
                if token_json.get('error') is not None:
                    return error_response(token_json['error']['message'])
                access_token = token_json['access_token']
                async with session.get(
                        f"https://graph.facebook.com/v2.5/me",
                        params={
                            "fields": "email",
                            "access_token": access_token}) as user_info:
                    user = await user_info.json()
                    user_id = user.get('id')
                    user_email = user.get('email')
                    found_user = await get_user_by_social(request.app['db'], users.facebookId, user_id)
                    if found_user is None:
                        return error_response("User was not found")
                    jwt_encoded_tokens = await get_services_tokens(request.app['db'], found_user)
                    return web.HTTPFound(f"/api/v1/session/create?tokens={jwt_encoded_tokens}")
        except Exception as e:
            return error_response(str(e))


async def plain(request: web.Request):
    """
    ---
    description: This end-point allow to auth user with email and password provided.
    tags:
    - Auth
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
    -   in: query
        name: email
        description: Users email to authenticate.
        schema:
            type: string
            example: win10@mail.ru
        required: true
    -   in: query
        name: password
        description: Users password to authenticate
        schema:
            type: string
            example: password
        required: true
    responses:
        "302":
            description: successful operation.
        "200":
            description: returns allowed 2fa methods to continue auth process.
        "400":
            description: wrong credentials
    """
    body_schema = {
        "type": "object",
        "properties": {
            "email": {"type": "string", "format": "email"},
            "password": {"type": "string"}
        },
        "required": [
            "email",
            "password"
        ]
    }
    try:
        db = request.app['db']
        body = dict(request.rel_url.query)
        try:
            validate(instance=body, schema=body_schema, format_checker=FormatChecker())
        except ValidationError as err:
            return error_response({
                "errors": err.message
            })
        user = await find_user(db, body['email'], body['password'])
        if user is None:
            return error_response("Wrong credentials", 400, 1030)
        jwt_encoded_tokens = await get_services_tokens(request.app['db'], user)
        return web.HTTPFound(f"/api/v1/session/create?tokens={jwt_encoded_tokens}")
    except Exception as e:
        return error_response(str(e))
