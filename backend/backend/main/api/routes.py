from aiohttp import web

from backend.main.api.v1 import routes as v1
from backend.utils.urls import include_urls


def init_routes(app: web.Application, prefix):
    include_urls(app, prefix + "/v1", v1)
